from django.urls import  path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("err", views.error_task_view, name="err"),
    path("suc", views.calculate_10_x_10_view, name="suc"),
]