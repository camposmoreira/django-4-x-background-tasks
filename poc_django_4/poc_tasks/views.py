from django.http import HttpResponse
from .task import task_with_error, calculate_10_x_10

def index(request):
    return HttpResponse("Hello, world. You're at the index.")


def error_task_view(request):
    task_with_error()
    return HttpResponse("Generate task with error")


def calculate_10_x_10_view(request):
    calculate_10_x_10()
    return HttpResponse("Generate task with success")