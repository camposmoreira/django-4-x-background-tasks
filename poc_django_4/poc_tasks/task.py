from background_task import background

@background
def task_with_error():
    raise ValueError("Error")

@background
def calculate_10_x_10():
    print("10 * 10 ", 100)